﻿namespace UI
{
    partial class Frm_SaveMoney
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_SaveMoney));
            this.txt_Result = new System.Windows.Forms.TextBox();
            this.lbl_Tip = new System.Windows.Forms.Label();
            this.Btn_Five = new System.Windows.Forms.Button();
            this.Btn_Three = new System.Windows.Forms.Button();
            this.Btn_Exit = new System.Windows.Forms.Button();
            this.Btn_Back = new System.Windows.Forms.Button();
            this.Btn_Sure = new System.Windows.Forms.Button();
            this.Btn_Fiveth = new System.Windows.Forms.Button();
            this.Btn_Twl = new System.Windows.Forms.Button();
            this.Btn_Ten = new System.Windows.Forms.Button();
            this.Btn_One = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_Result
            // 
            this.txt_Result.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_Result.Location = new System.Drawing.Point(12, 186);
            this.txt_Result.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txt_Result.Multiline = true;
            this.txt_Result.Name = "txt_Result";
            this.txt_Result.Size = new System.Drawing.Size(152, 34);
            this.txt_Result.TabIndex = 28;
            // 
            // lbl_Tip
            // 
            this.lbl_Tip.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_Tip.Location = new System.Drawing.Point(9, 151);
            this.lbl_Tip.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Tip.Name = "lbl_Tip";
            this.lbl_Tip.Size = new System.Drawing.Size(154, 33);
            this.lbl_Tip.TabIndex = 27;
            this.lbl_Tip.Text = "请输入所存放的金额：\r\n( 金额是100整数倍）";
            // 
            // Btn_Five
            // 
            this.Btn_Five.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Btn_Five.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btn_Five.Location = new System.Drawing.Point(233, 36);
            this.Btn_Five.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Btn_Five.Name = "Btn_Five";
            this.Btn_Five.Size = new System.Drawing.Size(81, 33);
            this.Btn_Five.TabIndex = 18;
            this.Btn_Five.Text = "500";
            this.Btn_Five.UseVisualStyleBackColor = true;
            this.Btn_Five.Click += new System.EventHandler(this.Btn_Five_Click);
            this.Btn_Five.MouseLeave += new System.EventHandler(this.Btn_Five_MouseLeave);
            this.Btn_Five.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Btn_Five_MouseMove);
            // 
            // Btn_Three
            // 
            this.Btn_Three.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Btn_Three.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btn_Three.Location = new System.Drawing.Point(119, 36);
            this.Btn_Three.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Btn_Three.Name = "Btn_Three";
            this.Btn_Three.Size = new System.Drawing.Size(81, 33);
            this.Btn_Three.TabIndex = 19;
            this.Btn_Three.Text = "300";
            this.Btn_Three.UseVisualStyleBackColor = true;
            this.Btn_Three.Click += new System.EventHandler(this.Btn_Five_Click);
            this.Btn_Three.MouseLeave += new System.EventHandler(this.Btn_Five_MouseLeave);
            this.Btn_Three.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Btn_Five_MouseMove);
            // 
            // Btn_Exit
            // 
            this.Btn_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Btn_Exit.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btn_Exit.Location = new System.Drawing.Point(166, 142);
            this.Btn_Exit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Btn_Exit.Name = "Btn_Exit";
            this.Btn_Exit.Size = new System.Drawing.Size(76, 33);
            this.Btn_Exit.TabIndex = 20;
            this.Btn_Exit.Text = "退卡";
            this.Btn_Exit.UseVisualStyleBackColor = true;
            this.Btn_Exit.Click += new System.EventHandler(this.Btn_Exit_Click);
            this.Btn_Exit.MouseLeave += new System.EventHandler(this.Btn_Five_MouseLeave);
            this.Btn_Exit.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Btn_Five_MouseMove);
            // 
            // Btn_Back
            // 
            this.Btn_Back.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Btn_Back.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btn_Back.Location = new System.Drawing.Point(247, 142);
            this.Btn_Back.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Btn_Back.Name = "Btn_Back";
            this.Btn_Back.Size = new System.Drawing.Size(71, 33);
            this.Btn_Back.TabIndex = 21;
            this.Btn_Back.Text = "返回";
            this.Btn_Back.UseVisualStyleBackColor = true;
            this.Btn_Back.Click += new System.EventHandler(this.Btn_Back_Click);
            this.Btn_Back.MouseLeave += new System.EventHandler(this.Btn_Five_MouseLeave);
            this.Btn_Back.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Btn_Five_MouseMove);
            // 
            // Btn_Sure
            // 
            this.Btn_Sure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Btn_Sure.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btn_Sure.Location = new System.Drawing.Point(167, 186);
            this.Btn_Sure.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Btn_Sure.Name = "Btn_Sure";
            this.Btn_Sure.Size = new System.Drawing.Size(151, 33);
            this.Btn_Sure.TabIndex = 22;
            this.Btn_Sure.Text = "确定";
            this.Btn_Sure.UseVisualStyleBackColor = true;
            this.Btn_Sure.Click += new System.EventHandler(this.Btn_Sure_Click);
            this.Btn_Sure.MouseLeave += new System.EventHandler(this.Btn_Five_MouseLeave);
            this.Btn_Sure.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Btn_Five_MouseMove);
            // 
            // Btn_Fiveth
            // 
            this.Btn_Fiveth.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Btn_Fiveth.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btn_Fiveth.Location = new System.Drawing.Point(233, 95);
            this.Btn_Fiveth.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Btn_Fiveth.Name = "Btn_Fiveth";
            this.Btn_Fiveth.Size = new System.Drawing.Size(81, 33);
            this.Btn_Fiveth.TabIndex = 23;
            this.Btn_Fiveth.Text = "5000";
            this.Btn_Fiveth.UseVisualStyleBackColor = true;
            this.Btn_Fiveth.Click += new System.EventHandler(this.Btn_Five_Click);
            this.Btn_Fiveth.MouseLeave += new System.EventHandler(this.Btn_Five_MouseLeave);
            this.Btn_Fiveth.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Btn_Five_MouseMove);
            // 
            // Btn_Twl
            // 
            this.Btn_Twl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Btn_Twl.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btn_Twl.Location = new System.Drawing.Point(119, 93);
            this.Btn_Twl.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Btn_Twl.Name = "Btn_Twl";
            this.Btn_Twl.Size = new System.Drawing.Size(81, 33);
            this.Btn_Twl.TabIndex = 24;
            this.Btn_Twl.Text = "2000";
            this.Btn_Twl.UseVisualStyleBackColor = true;
            this.Btn_Twl.Click += new System.EventHandler(this.Btn_Five_Click);
            this.Btn_Twl.MouseLeave += new System.EventHandler(this.Btn_Five_MouseLeave);
            this.Btn_Twl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Btn_Five_MouseMove);
            // 
            // Btn_Ten
            // 
            this.Btn_Ten.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Btn_Ten.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btn_Ten.Location = new System.Drawing.Point(9, 93);
            this.Btn_Ten.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Btn_Ten.Name = "Btn_Ten";
            this.Btn_Ten.Size = new System.Drawing.Size(81, 33);
            this.Btn_Ten.TabIndex = 25;
            this.Btn_Ten.Text = "1000";
            this.Btn_Ten.UseVisualStyleBackColor = true;
            this.Btn_Ten.Click += new System.EventHandler(this.Btn_Five_Click);
            this.Btn_Ten.MouseLeave += new System.EventHandler(this.Btn_Five_MouseLeave);
            this.Btn_Ten.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Btn_Five_MouseMove);
            // 
            // Btn_One
            // 
            this.Btn_One.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Btn_One.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btn_One.Location = new System.Drawing.Point(9, 38);
            this.Btn_One.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Btn_One.Name = "Btn_One";
            this.Btn_One.Size = new System.Drawing.Size(81, 33);
            this.Btn_One.TabIndex = 26;
            this.Btn_One.Text = "100";
            this.Btn_One.UseVisualStyleBackColor = true;
            this.Btn_One.Click += new System.EventHandler(this.Btn_Five_Click);
            this.Btn_One.MouseLeave += new System.EventHandler(this.Btn_Five_MouseLeave);
            this.Btn_One.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Btn_Five_MouseMove);
            // 
            // Frm_SaveMoney
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(326, 232);
            this.Controls.Add(this.txt_Result);
            this.Controls.Add(this.lbl_Tip);
            this.Controls.Add(this.Btn_Five);
            this.Controls.Add(this.Btn_Three);
            this.Controls.Add(this.Btn_Exit);
            this.Controls.Add(this.Btn_Back);
            this.Controls.Add(this.Btn_Sure);
            this.Controls.Add(this.Btn_Fiveth);
            this.Controls.Add(this.Btn_Twl);
            this.Controls.Add(this.Btn_Ten);
            this.Controls.Add(this.Btn_One);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.Name = "Frm_SaveMoney";
            this.Text = "存款界面";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_SaveMoney_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_Result;
        private System.Windows.Forms.Label lbl_Tip;
        private System.Windows.Forms.Button Btn_Five;
        private System.Windows.Forms.Button Btn_Three;
        private System.Windows.Forms.Button Btn_Exit;
        private System.Windows.Forms.Button Btn_Back;
        private System.Windows.Forms.Button Btn_Sure;
        private System.Windows.Forms.Button Btn_Fiveth;
        private System.Windows.Forms.Button Btn_Twl;
        private System.Windows.Forms.Button Btn_Ten;
        private System.Windows.Forms.Button Btn_One;
    }
}